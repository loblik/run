#ifndef IDENT_TABLE_H
#define IDENT_TABLE_H

struct ident_table {
	char **name;
	int allocated;
	int size;
};

struct ident_table *ident_table_copy(struct ident_table *ident);
void ident_table_print(struct ident_table *table);
struct ident_table *ident_table_alloc();
void ident_table_init(struct ident_table *table);
int ident_table_contains(struct ident_table *table, char *ident);
int ident_table_index(struct ident_table *table, char *ident);
void ident_table_free(struct ident_table *table);
#endif
