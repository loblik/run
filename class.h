#ifndef CLASS_H
#define CLASS_H

#include <stdlib.h>
#include <string.h>
#include "cons.h"
#include "code.h"
#include "hash.h"
#include "ident.h"

struct class {
	char *this_name;
	char *super_name;
	struct class *super;
	struct cons_table cons;
	struct ident_table fds;
	HTable procs;
};

struct proc {
	char *name;
	int num_args;
	int locals;
	CODE code;
};

typedef struct proc PROC;
typedef struct class CLASS;

struct Inst *proc_get_first_inst(PROC *p);
int class_num_fields(CLASS *c);
PROC *class_lookup_proc(CLASS **target, const char *name);
void class_init(CLASS *c);
void class_free(void *data);
void class_add_proc(CLASS *c, PROC *p);
void proc_free(void *data);
#endif
