#ifndef CODE_H
#define CODE_H

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#define FOREACH_INST(OP) \
		OP(PUSH_LOCAL)\
		OP(PUSH_IVAR)\
		OP(PUSH_CONST)\
		OP(PUSH_SELF)\
		OP(SET_IVAR)\
		OP(SET_LOCAL)\
		OP(CALL)\
		OP(CALL_SUPER)\
		OP(CALL_NATIVE)\
		OP(JCMP)\
		OP(JMP)\
		OP(POP)\
		OP(NOP)\
		OP(RET)\

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

enum Iop {
	FOREACH_INST(GENERATE_ENUM)
};

extern const char *inst_str[];

struct Inst {
	enum Iop opcode;
	void *op1;
	void *op2;
};

struct Code {
	int allocated_size;
	int used_size;
	struct Inst *c;
};

typedef struct Code CODE;
typedef struct Inst INST;

struct Code *code_alloc(void);

struct Inst inst_parse(const char*);
void inst_sprintf(struct Inst *i, char *buffer);
void inst_sprintf_int(struct Inst *i, char *buffer);

void code_init(struct Code *c);
int code_pushi(struct Code *c, struct Inst i);
int code_insert_at(struct Code *c, struct Inst i, int pc);
int code_push_nop(struct Code *c);
void code_add_ret(struct Code *c);
void code_print(struct Code *c);
void code_free(struct Code *c);
int code_get_pc(struct Code *c);
#endif
