#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <math.h>
#include <string.h>
#include <stdint.h>
#include "alloc.h"
#include "vm.h"
#include "debug.h"

#define SIZE (4096*8)

/* TODO: this is platform dependent code, ifdef? */
static unsigned int next_power(uint64_t v) {
	/* taken from: http://graphics.stanford.edu/~seander/bithacks.html */
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v |= v >> 32;
	v++;
	return v;
}

struct blk *blk_create(void *addr) {
	struct blk *b = (struct blk*)malloc(sizeof(struct blk));
	b->addr = addr;
	b->next = NULL;
	return b;
}

struct blk *blk_remove_first(struct blk **next) {
	struct blk *removed = *next;
	*next = removed->next;
	return removed;
}

void blk_append_first(struct blk **next, struct blk *new) {
	new->next = *next;
	*next = new;
}

struct blk *blk_make_buddy(struct blk *b, int size) {
	return blk_create(b->addr + size);
}

void blk_free_list(struct blk *b) {
	struct blk *next;
	while(b) {
		next = b->next;
		free(b);
		b = next;
	}
}

void blk_print_list(struct blk *b) {
	for (; b != NULL; b = b->next) {
		printf("%p ", b->addr);
	}
}

void alloc_print_lists(struct alloc *al) {
	int i;
	printf("FREE\n");
	for (i = 0; i < al->list_count; i++) {
		printf("%d (%d): ", i, al->list_size[i]);
		blk_print_list(al->free_list[i]);
		printf("\n");
	}
	printf("USED\n");
	for (i = 0; i < al->list_count; i++) {
		printf("%d (%d): ", i, al->list_size[i]);
		blk_print_list(al->used_list[i]);
		printf("\n");
	}
}

/* check if two blocks are buddies ang can be merged */
void *test_buddies(struct alloc *al, void *a1, void *a2, int order) {
	int size = al->list_size[order];
	if (a2 < a1) {
		void *tmp = a2;
		a2 = a1;
		a1 = tmp;
	}
	if ((a1 + size) == a2 &&
		((((size_t)a1 >> (order + 1)) << (order + 1)) == (size_t)a1)) {
		return a1;
	} else {
		return NULL;
	}
}

/* merge block recursivelly */
void alloc_merge_buddy(struct alloc *al, struct blk *block, int order) {
	struct blk **last_next = &al->free_list[order];
	struct blk *p = al->free_list[order];

	while (p) {
		void *buddy = test_buddies(al, p->addr, block->addr, order);
		if (buddy) {
			*last_next = p->next;
			free(p);
			block->addr = buddy;
			alloc_merge_buddy(al, block, order + 1);
			return;
		}
		last_next = &p->next;
		p = p->next;
	}
	*last_next = block;
	block->next = NULL;
}

void alloc_collect(struct alloc *al) {
	int i;
	struct blk *p;
	struct blk **last_next;
	for (i = 0; i < al->list_count; i++) {
		p = al->used_list[i];
		last_next = &al->used_list[i];
		while (p) {
			if (!object_marked(p->addr)) {
				object_freed(p->addr);
#ifdef _DEBUG
			if (debug)
				printf("free: %p\n", p->addr);
#endif
				*last_next = p->next;
				alloc_merge_buddy(al, p, i);
				return alloc_collect(al);
			}
			last_next = &p->next;
			p = p->next;
		}
	}
}

void alloc_unmark(struct alloc *al) {
	int i;
	struct blk *p;
	for (i = 0; i < al->list_count; i++) {
		p = al->used_list[i];
		while (p) {
			object_unmark(p->addr);
			p = p->next;
		}
	}
}

void *alloc_allocate(struct alloc *al, int size) {
	int list_id = log2(next_power(size));
	if (list_id >= al->list_count || size < 0)
		return NULL;
	struct blk *new_chunk = NULL;
	if (al->free_list[list_id] != NULL) {
		new_chunk = blk_remove_first(&al->free_list[list_id]);
	} else {
		struct blk *to_split;
		int i;
		for (i = list_id; i < al->list_count; i++)
			if (al->free_list[i] != NULL)
				break;
		if (i >= al->list_count)
			return NULL;
		to_split = blk_remove_first(&al->free_list[i]);
		for (i--; i >= list_id; i--) {
			struct blk *second_half = blk_make_buddy(to_split, al->list_size[i]);
			blk_append_first(&al->free_list[i], to_split);
			to_split = second_half;
		}
		new_chunk = to_split;
	}
	blk_append_first(&al->used_list[list_id], new_chunk);
	return new_chunk->addr;
}

void alloc_list_add(struct alloc *al, int list_id, void *addr) {
	struct blk *b = (struct blk*)malloc(sizeof(struct blk));
	b->addr = addr;
	b->next = NULL;
	if (al->free_list[list_id]) {
		al->free_list[list_id] = b;
	}
}

void alloc_free(struct alloc *al) {
	int i;
	for (i = 0; i < al->list_count; i++) {
		blk_free_list(al->free_list[i]);
		blk_free_list(al->used_list[i]);
	}
	free(al->free_list);
	free(al->used_list);
	free(al->list_size);
	free(al->base);
}

void alloc_init(struct alloc *al) {
	al->base = NULL;
	int size = SIZE*sizeof(void*);
	int ret = posix_memalign(&al->base, size, size);
	if (ret != 0) {
		switch (ret) {
			case EINVAL:
				perror("posix_memalign");
			break;
		}
		exit(EXIT_FAILURE);
	}
#ifdef _MEM
	printf("memory size: %d\n", size);
	printf("memory pool: %p - %p\n", al->base, al->base + size);
#endif
	al->list_count = log2(size) + 1;
	al->free_list = (struct blk**)malloc(al->list_count*sizeof(struct blk));
	al->used_list = (struct blk**)malloc(al->list_count*sizeof(struct blk));
	al->list_size = (int*)malloc(al->list_count*sizeof(int));
	memset(al->free_list, 0, al->list_count*sizeof(struct blk**));
	memset(al->used_list, 0, al->list_count*sizeof(struct blk**));
	int i;
	for (i = 0; i < al->list_count; i++) {
		al->list_size[i] = (int)pow(2, i);
	}
	al->free_list[al->list_count - 1] = blk_create(al->base);
}
