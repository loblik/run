#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "ident.h"

#define IDENT_TABLE_INITIAL_SIZE	8
#define IDENT_TABLE_RESIZE_FACTOR	2

struct ident_table *ident_table_alloc() {
	struct ident_table *new = (struct ident_table*)malloc(sizeof(struct ident_table));
	ident_table_init(new);
	return new;
}

void ident_table_init(struct ident_table *table) {
	table->name = (char**)malloc(sizeof(char*)*IDENT_TABLE_RESIZE_FACTOR);
	table->size = 0;
	table->allocated = IDENT_TABLE_RESIZE_FACTOR;
}

int ident_table_contains(struct ident_table *table, char *ident) {
	int i;
	for (i = 0; i < table->size; i++)
		if (strcmp(table->name[i], ident) == 0)
			return 1;
	return 0;
}

int ident_table_index(struct ident_table *table, char *ident) {
	int i;
	for (i = 0; i < table->size; i++)
		if (strcmp(table->name[i], ident) == 0)
			return i;

	if (table->size == table->allocated) {
		table->allocated *= IDENT_TABLE_RESIZE_FACTOR;
		table->name = (char**)realloc(table->name, sizeof(char*)*table->allocated);
	}

	table->name[table->size] = strdup(ident);
	return table->size++;
}

struct ident_table *ident_table_copy(struct ident_table *ident) {
	struct ident_table *copy = ident_table_alloc();
	int i;
	for (i = 0; i < ident->size; i++)
		ident_table_index(copy, ident->name[i]);
	return copy;
}

void ident_table_free(struct ident_table *table) {
	int i;
	for (i = 0; i < table->size; i++)
		free(table->name[i]);
	free(table->name);
}

void ident_table_print(struct ident_table *table) {
	int i;
	for (i = 0; i < table->size; i++) {
		printf("%s\n", table->name[i]);
	}
}
