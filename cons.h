#ifndef CONS_H
#define CONS_H

#define FOREACH_CTYPE(OP) \
		OP(C_INT)\
		OP(C_STRING)\
		OP(C_CLASS)\
		OP(C_METHOD)\
		OP(C_NIL)\

#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_STRING(STRING) #STRING,

#define TABLE_INITIAL_SIZE		32
#define TABLE_RESIZE_FACTOR		2

enum CType {
	FOREACH_CTYPE(GENERATE_ENUM)
};

extern const char *cons_type_str[];

struct literal {
	enum CType type;
	char *name;
	void *obj;
};

struct cons_table {
	struct literal *table;
	int size;
	int allocated;
};

const char *cons_table_name(struct cons_table *ct, int i);
void *cons_table_obj(struct cons_table *ct, int i);
void cons_table_print(struct cons_table *ct);
struct literal literal_parse(char *type, char *value);
void cons_table_free(struct cons_table* new);
struct cons_table* cons_table_alloc();
void cons_table_init(struct cons_table* new);
int cons_table_add(struct cons_table*, struct literal);
void cons_table_for_each(struct cons_table*, void (*callback)(struct literal, void*), void *data);
#endif
