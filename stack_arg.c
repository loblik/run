#include <stdlib.h>
#include <stdio.h>
#include "stack_arg.h"

void stack_arg_init(struct stack_arg *stack) {
	stack->allocated = STACK_INITIAL_SIZE;
	stack->top = 0;
	stack->data = (void**)malloc(sizeof(void*)*stack->allocated);
}

void stack_arg_alloc(struct stack_arg *stack, int size) {
	int i;
	for (i = 0; i < size; i++)
		stack_arg_push(stack, NULL);
}

void *stack_arg_top(struct stack_arg *stack) {
	if (stack->top == 0)
		return NULL;
	return stack->data[stack->top-1];
}

void *stack_arg_pop(struct stack_arg *stack) {
	return stack->data[--stack->top];
}

void stack_arg_free(struct stack_arg *stack) {
	free(stack->data);
}

void *stack_arg_get(struct stack_arg *stack, int index) {
	return stack->data[index];
}

void stack_arg_set(struct stack_arg *stack, int index, void *val) {
	stack->data[index] = val;
}

void stack_arg_print(struct stack_arg *stack) {
	int i;
	for (i = 0; i < stack->top; i++)
		printf("%i: %p\n", i, stack->data[i]);
}

void stack_arg_push(struct stack_arg *stack, void *arg) {
	if (stack->allocated == stack->top) {
		stack->allocated *= STACK_GROWTH_FACTOR;
		stack->data = (void**)realloc(stack->data, sizeof(void*)*stack->allocated);
	}
	stack->data[stack->top++] = arg;
}
