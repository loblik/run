%option yylineno
%option nounput
%option noinput
%{
#include "parser.h"

int colnum = 1;

#define YY_USER_ACTION {yylloc.first_line = yylineno; \
        yylloc.first_column = colnum;                 \
        colnum=colnum+yyleng;                         \
        yylloc.last_column=colnum;                    \
        yylloc.last_line = yylineno;}
%}

cr              [\t ]*[\n]+
blanks          [ \t]+
comment			#.*\n+
escape			\\\n
native			_[A-Za-z_]*
identifier		[a-zA-Z][_a-zA-Z0-9]*
integer         [0-9]+
kwend           end
classident		[A-Z][_a-zA-Z0-9]*
string          L?\"(\\.|[^\\"])*\"
%%

{blanks}        { /* ignore */ }
{comment}        { /* ignore */ }
{escape}		{ }

{cr}            { colnum = 1; return(CR); }
";"             return(SEMI);

"return"        return(KW_RETURN);
"class"         return(KW_CLASS);
{kwend}         return(KW_END);
"if"            return(KW_IF);
"then"          return(KW_THEN);
"else"          return(KW_ELSE);
"while"         return(KW_WHILE);
"super"         return(KW_SUPER);
"continue"		return(KW_CONTINUE);
"break"			return(KW_BREAK);

"nil"			return(NIL);

"["				return(LBR);
"]"				return(RBR);
"("             return(LPAR);
")"             return(RPAR);

"fnc"     		return(PROCEDURE);
"."             return(DOT);
","             return(COMMA);


"+"             return(BIN_PLUS);
"-"             return(BIN_MIN);
"*"             return(BIN_MUL);
"/"             return(BIN_DIV);
"="             return(BIN_ASSIGN);

">"				return(BIN_GT);
">="			return(BIN_GE);
"<"				return(BIN_LT);
"<="			return(BIN_LE);
"=="			return(BIN_EQ);
"!="			return(BIN_NE);

"&&"			return(BIN_AND);
"||"			return(BIN_OR);


{string}        {
                int buffer_len  = strlen(yytext) + 1 - 2 + 0; // - 2 x "
				yylval.sval = malloc(buffer_len*sizeof(char));
				strncpy(yylval.sval + 0, yytext + 1, buffer_len - 1);
//				yylval.sval[0] = '\'';
				yylval.sval[buffer_len - 1] = '\0';
//				yylval.sval[buffer_len - 2] = '\'';
				return(STRING);
}

{integer}       {
                int buffer_len  = strlen(yytext) + 1;
				yylval.sval = malloc(buffer_len*sizeof(char));
				strncpy(yylval.sval, yytext, buffer_len);
                return(INTEGER);
}

{classident}	{
                int buffer_len  = strlen(yytext) + 1;
				yylval.sval = malloc(buffer_len*sizeof(char));
				strncpy(yylval.sval, yytext, buffer_len);
				return(CLASS_IDENTIFIER);
}

{native}	{
                int buffer_len  = strlen(yytext) + 1;
				yylval.sval = malloc(buffer_len*sizeof(char));
				strncpy(yylval.sval, yytext, buffer_len);
				return(NATIVE);
}


{identifier}	{
                int buffer_len  = strlen(yytext) + 1;
				yylval.sval = malloc(buffer_len*sizeof(char));
				strncpy(yylval.sval, yytext, buffer_len);
				return(IDENTIFIER);
}

.               {
printf("%d:%d: unknown character: %c\n", yylineno, yylloc.first_column, yytext[0]);
    exit(EXIT_FAILURE);
 }

