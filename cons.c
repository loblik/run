#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "cons.h"

const char *cons_type_str[] = {
	FOREACH_CTYPE(GENERATE_STRING)
};

struct literal literal_parse(char *type, char *value) {
	int i;
	struct literal new_literal;
	new_literal.name = value;
	if (new_literal.name)
		new_literal.name = strdup(new_literal.name);
	for (i = 0; i < sizeof(cons_type_str)/sizeof(cons_type_str[0]); i++) {
		if (strcmp(cons_type_str[i], type) == 0) {
			new_literal.type = i;
		}
	}
	new_literal.obj = NULL;
	return new_literal;
}

void *cons_table_obj(struct cons_table *ct, int i) {
	return ct->table[i].obj;
}

const char *cons_table_name(struct cons_table *ct, int i) {
	return ct->table[i].name;
}

void cons_table_print(struct cons_table *ct) {
	int i;
	for (i = 0; i < ct->size; i++) {
		printf("%3d: %s %s\n", i, cons_type_str[ct->table[i].type], ct->table[i].name);
	}
}

int cons_table_add(struct cons_table *ct, struct literal new) {
	int i;
	for (i = 0; i < ct->size; i++)
		if (ct->table[i].type == new.type && strcmp(new.name, ct->table[i].name) == 0) {
			free(new.name);
			return i;
		}

	if (ct->allocated == ct->size) {
		ct->allocated *= TABLE_RESIZE_FACTOR;
		ct->table = (struct literal*)realloc(ct->table, sizeof(struct literal)*ct->allocated);
	}
	new.obj = NULL;
	ct->table[ct->size] = new;
	return ct->size++;
}

struct cons_table *cons_table_alloc() {
	struct cons_table *new = (struct cons_table*)malloc(sizeof(struct cons_table));
	cons_table_init(new);
	return new;
}

void cons_table_free(struct cons_table *tab) {
	int i;
	for (i = 0; i < tab->size; i++) {
		free(tab->table[i].name);
	}
	free(tab->table);
}

void cons_table_init(struct cons_table *new) {
	new->table = (struct literal*)malloc(sizeof(struct literal)*TABLE_INITIAL_SIZE);
	new->size = 0;
	new->allocated = TABLE_INITIAL_SIZE;
}

void cons_table_for_each(struct cons_table *ct,
		void (*callback)(struct literal, void*),
		void *data) {
	int i;
	for (i = 0; i < ct->size; i++)
		callback(ct->table[i], data);
}
