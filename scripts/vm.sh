#!/bin/bash

# compiler
CMP=compile
# interpreter
VM=vm

file=$PWD/$1

classes=`$CMP $file`
ret=$?
if [ $ret -ne 0 ]; then
	echo "$classes" # print error message
	exit $ret
fi

full_path=
for i in $classes; do
	full_path="$full_path $PWD/$i"
done

shift
$VM $full_path - $@

exit $?
