#ifndef DEBUG_H
#define DEBUG_H

#include <stdlib.h>

#ifdef _DEBUG

extern int debug;

#define DEBUG_INIT() { debug = getenv("VM_DEBUG") != NULL; }
#define DEBUG_PRINT(...) { if (debug) fprintf(stderr, __VA_ARGS__); }

#else

#define DEBUG_PRINT(...) ;
#define DEBUG_INIT() ;

#endif

#endif

