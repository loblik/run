#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <gmp.h>

#include "jsmn.h"
#include "hash.h"
#include "code.h"
#include "cons.h"
#include "stack_arg.h"
#include "alloc.h"
#include "native.h"
#include "class_parse.h"
#include "vm.h"
#include "class.h"
#include "debug.h"

#ifdef _DEBUG
int debug;
#endif

#define VM_PERROR_EXIT(...) { fprintf(stderr, __VA_ARGS__); exit(EXIT_FAILURE); }

#define VM_PERROR_BT_EXIT(vm, ...) {\
	vm_print_bt(vm); fprintf(stderr, __VA_ARGS__); exit(EXIT_FAILURE); }

void iprint(struct Inst *i) {
	char buffer[256];
	inst_sprintf_int(i, buffer);
	DEBUG_PRINT("%s\n", buffer);
}

struct vm;
struct object;

void class_print(CLASS *c);

struct header {
	CLASS *class;
	int flags;
};

struct object {
	struct header head;
	union {
		mpz_t big_num;
		char str;
		struct object *field[1];
		struct {
			int size;
			struct object *data[1];
		} array;
	};
};

typedef struct object OBJECT;

struct stack_call_entry {
	OBJECT *this;
	int ebp;
	struct Inst *pc;
	CLASS *current_class;
	/* this is for better error reprots */
	PROC *proc;
};

typedef struct stack_call_entry CSTACK_ENTRY;

struct stack_call {
	CSTACK_ENTRY *data;
	int top;
	int allocated;
};
typedef struct stack_call CSTACK;

typedef struct {
	HTable classes;
	struct alloc mem_alloc;
	struct stack_arg st_arg;
	struct stack_call st_call;
	struct Inst *pc;
	CLASS *current;
	PROC *curr_proc;
	OBJECT *this;
	int ebp;
} VM;

typedef struct stack_arg ASTACK;

static void (*native_function[])(VM *vm);

void vm_print_bt(VM *vm);

void object_freed(void *addr) {
	OBJECT *o = (OBJECT*)addr;
	if (o->head.flags & FLAG_INT) {
		mpz_clear(o->big_num);
	}
}

int object_marked(void *addr) {
	OBJECT *o = (OBJECT*)addr;
	return o->head.flags & FLAG_MARK;
}

void object_mark(void *addr) {
	OBJECT *o = (OBJECT*)addr;
	o->head.flags |= FLAG_MARK;
}

void object_unmark(void *addr) {
	OBJECT *o = (OBJECT*)addr;
	o->head.flags &= ~(FLAG_MARK);
}

void vm_gg_mark(OBJECT *o, VM *vm) {
	if (o == NULL || object_marked(o))
		return;

	int flag = o->head.flags;
	switch(flag) {
		case FLAG_INT:
		case FLAG_NIL:
		case FLAG_STRING:
			DEBUG_PRINT("plain mark: %p\n", o);
			object_mark(o); break;
		case FLAG_ARRAY: {
				DEBUG_PRINT("array mark: %p\n", o);
				object_mark(o);
				int size = o->array.size;
				int i;
				for (i = 0; i < size; i++) {
					DEBUG_PRINT("array submark: %p\n", o->array.data[i]);
					vm_gg_mark(o->array.data[i], vm);
				}
				DEBUG_PRINT("array mark end\n");
			}
			break;
		case FLAG_OBJECT: {
				DEBUG_PRINT("object mark: %p %s\n", o, o->head.class->this_name);
				object_mark(o);
				int fields = o->head.class->fds.size;
				int i;
				for (i = 0; i < fields; i++)
					if (o->field[i]) {
						DEBUG_PRINT("  marking: %d %p\n", i, o->field[i] );
						vm_gg_mark(o->field[i], vm);
					}
			}
			break;
		default:
			printf("flags: %d %p\n", o->head.flags, o);
			alloc_print_lists(&vm->mem_alloc);
			printf("mark: unknwon object %p %d \n",o,  o->head.flags);
			exit(EXIT_FAILURE);
			break;
	}
}

void vm_gg_collect(VM *vm, int full) {

	HTIter it;
	CLASS *c;
	htable_iterator_init(&vm->classes, &it);
	while((c = (CLASS*)htable_iterator_obj(&it))) {
		int i;
		for (i = 0; i < c->cons.size; i++) {
			OBJECT *o = c->cons.table[i].obj;
			if (o) {
				vm_gg_mark(c->cons.table[i].obj, vm);
			}
		}
		htable_iterator_next(&vm->classes, &it);
	}

	CSTACK *cstack = &vm->st_call;
	int i;
	for (i = 0; i < cstack->top; i++) {
		DEBUG_PRINT("C-MARK: %d %p\n", i, cstack->data[i].this);
		vm_gg_mark(cstack->data[i].this, vm);
	}

	DEBUG_PRINT("THIS-MARK: %p\n", vm->this);
	vm_gg_mark(vm->this, vm);

	DEBUG_PRINT("END-MARKING\n");
	ASTACK *astack = &vm->st_arg;
	for (i = 0; i < astack->top; i++) {
		if (astack->data[i]) {
			DEBUG_PRINT("A-MARK: %d %p\n", i, astack->data[i]);
			vm_gg_mark(astack->data[i], vm);
		}
	}

	alloc_collect(&vm->mem_alloc);
	alloc_unmark(&vm->mem_alloc);
}


static const char *native_classes[] = {
	"Class.class",
	"Int.class",
	"String.class",
	"Void.class",
	"Sys.class",
	"Array.class",
	"Nil.class"
};

void stack_call_init(CSTACK *stack) {
	stack->allocated = STACK_INITIAL_SIZE;
	stack->top = 0;
	stack->data = (CSTACK_ENTRY*)malloc(sizeof(CSTACK_ENTRY)*stack->allocated);
}

CSTACK_ENTRY *stack_call_top(CSTACK *stack) {
	return &stack->data[stack->top-1];
}

CSTACK_ENTRY stack_call_pop(CSTACK *stack) {
	return stack->data[--stack->top];
}

void stack_call_free(CSTACK *stack) {
	free(stack->data);
}

#ifdef _DEBUG
void stack_call_print(CSTACK *stack) {
	int i;
	for (i = 0; i < stack->top; i++) {
		CSTACK_ENTRY *e = &stack->data[i];
		DEBUG_PRINT("%i: c: %s pc: %p\n", i, e->this->head.class->this_name, e->pc);
	}
}
#endif

void stack_call_push(CSTACK *stack, CSTACK_ENTRY record) {
	if (stack->allocated == stack->top) {
		stack->allocated *= STACK_GROWTH_FACTOR;
		stack->data = (CSTACK_ENTRY*)realloc(stack->data, sizeof(CSTACK_ENTRY)*stack->allocated);
	}
	stack->data[stack->top++] = record;
}

/* replace escapes with appropriate charaters */
char *str_escape(const char *source) {
	int length = strlen(source);
	char *escaped = (char*)malloc(sizeof(char)*length + 1);
	char *out = escaped;
	int last = 0;
	while (*source != '\0') {
		if (*source == '\\') {
			last = !last;
		}
		if (last)
			switch (*source) {
				case '\\':
					source++;
					break;
				case 't':
					*out++ = '\t'; source++; last = 0;
					break;
				case 'n':
					*out++ = '\n'; source++; last = 0;
					break;
				case 'r':
					*out++ = '\r'; source++; last = 0;
					break;
				case 'f':
					*out++ = '\f'; source++; last = 0;
					break;
				case 'b':
					*out++ = '\b'; source++; last = 0;
					break;
				default:
					*out++ = *source++; last = 0;
					break;
			}
		else
			*out++ = *source++;
	}
	*out = '\0';
	return escaped;
}

CLASS *object_get_class(OBJECT *obj) {
	return obj->head.class;
}

CLASS *vm_class_find_hard(VM *vm, const char *name) {
	CLASS *c = (CLASS*)htable_find(&vm->classes, name);
	if (c == NULL)
		VM_PERROR_EXIT("unable to locate class %s\n", name);
	return c;
}

char *object_get_string(OBJECT *obj) {
	return &obj->str;
}

void vm_gg_collect(VM*, int);

void *vm_mem_alloc(VM *vm, int size) {
	void *ret = alloc_allocate(&vm->mem_alloc, size);
	if (ret == NULL) {
		/* try to collect some garbage */
		vm_gg_collect(vm, 1);
		/* let's try againg */
		ret = alloc_allocate(&vm->mem_alloc, size);
		if (ret == NULL) {
			VM_PERROR_BT_EXIT(vm, "lack of memory: requesting block %d bytes\n", size);
			alloc_print_lists(&vm->mem_alloc);
		}
#ifdef _DEBUG
		if (getenv("VM_DEBUG_MEM")) {
			printf("memory collected and reused\n");
		}
#endif
	}
	return ret;
}

struct object *vm_create_int(VM *vm) {
	int bytes = sizeof(struct header) + sizeof(int);
	struct object *new_int = (struct object*)vm_mem_alloc(vm, bytes);
	new_int->head.class = vm_class_find_hard(vm, "Int");
	mpz_init(new_int->big_num);
	new_int->head.flags = 0 | FLAG_INT;
	return new_int;
}

struct object *vm_create_int_from_int(VM *vm, int val) {
	struct object *new_int = vm_create_int(vm);
	mpz_set_si(new_int->big_num, val);
	return new_int;
}

struct object *vm_create_int_from_string(VM *vm, const char* val) {
	struct object *new_int = vm_create_int(vm);
	if (mpz_set_str(new_int->big_num, val, 0) != 0)
		VM_PERROR_BT_EXIT(vm, "can't convert string to int\n");
	return new_int;
}


struct object *vm_create_string(VM *vm, const char *val) {
	int bytes = sizeof(struct header) + sizeof(char)*(strlen(val)+1);
	struct object *new_string = (struct object*)vm_mem_alloc(vm, bytes);
	strcpy(&new_string->str, val);
	new_string->head.flags = 0 | FLAG_STRING;
	new_string->head.class = vm_class_find_hard(vm, "String");
	return new_string;
}

struct object *vm_create_instance(VM *vm, const char *name);

struct object *vm_create_nil(VM *vm) {
	OBJECT *nil = vm_create_instance(vm, "Nil");
	nil->head.flags = FLAG_NIL;
	return nil;
}

void vm_print_bt(VM *vm) {
	/* this mess actually prints nice backtrace */
	CSTACK *cstack = &vm->st_call;
	int i;
	for (i = 0; i < cstack->top; i++) {
		CSTACK_ENTRY *entry = &cstack->data[i];
		printf("%2d: [%p] %s.%s",i, entry->this,
				entry->current_class->this_name, entry->proc->name);
		int j;
		printf("(");
		for (j = 0; j < entry->proc->num_args; j++) {
			OBJECT *arg = (OBJECT*)vm->st_arg.data[entry->ebp + j];
			printf("%s[%p]", arg->head.class->this_name, arg);
			if (j < entry->proc->num_args - 1)
				printf(", ");
		}
		printf(")\n");
	}
	printf("%2d: [%p] %s.%s",i , vm->this, vm->current->this_name, vm->curr_proc->name);
	int j;
	printf("(");
	for (j = 0; j < vm->curr_proc->num_args; j++) {
		OBJECT *arg = (OBJECT*)vm->st_arg.data[vm->ebp + j];
		printf("%s[%p]", arg->head.class->this_name, arg);
		if (j < vm->curr_proc->num_args - 1)
			printf(", ");
	}
	printf(")\n");
}

struct object *vm_create_instance(VM *vm, const char *name) {
	CLASS *c = vm_class_find_hard(vm, name);
	int fields = class_num_fields(c);
	int bytes = sizeof(struct header) + sizeof(void*)*fields;
	struct object *new_inst = (struct object*)vm_mem_alloc(vm, bytes);
	DEBUG_PRINT("created instance: %s %p\n", name, new_inst);
	new_inst->head.flags = 0 | FLAG_OBJECT;
	new_inst->head.class = c;
	int i;
	for (i = 0; i < fields; i++) {
		new_inst->field[i] = vm_create_nil(vm);
	}
	return new_inst;
}

struct object *vm_create_array(VM *vm, int size) {
	int bytes = sizeof(struct header) + sizeof(int) + sizeof(void*)*size;
	struct object *new_array = (struct object*)vm_mem_alloc(vm, bytes);
	new_array->array.size = 0;
	new_array->head.flags = 0 | FLAG_ARRAY;
	new_array->head.class = vm_class_find_hard(vm, "Void");
	int i;
	OBJECT *nil = vm_create_nil(vm);
	for (i = 0; i < size; i++) {
		new_array->array.data[i] = nil;
		new_array->array.size++;
	}
	return new_array;
}

struct object *vm_create_class(VM *vm, const char *name) {
	CLASS *c = vm_class_find_hard(vm, name);
	int fields = class_num_fields(c);
	struct object *heap_num_fields = vm_create_int_from_int(vm, fields);
	struct object *heap_name = vm_create_string(vm, name);
	int bytes = sizeof(struct header) + sizeof(void*);
	struct object *new_class_inst = (struct object*)vm_mem_alloc(vm, bytes);
	new_class_inst->head.flags = 0 | FLAG_OBJECT;
	new_class_inst->field[0] = heap_num_fields;
	new_class_inst->field[1] = heap_name;
	DEBUG_PRINT("created class: %p %p  %p\n", heap_num_fields, heap_name, new_class_inst);
	new_class_inst->head.class = vm_class_find_hard(vm, "Class");
	return new_class_inst;
}

void vm_init(VM *vm) {
	htable_init(&vm->classes, &malloc, &class_free);
	alloc_init(&vm->mem_alloc);
	stack_arg_init(&vm->st_arg);
	stack_call_init(&vm->st_call);
}

void vm_free(VM *vm) {
	htable_free(&vm->classes);
	alloc_free(&vm->mem_alloc);
	stack_arg_free(&vm->st_arg);
	stack_call_free(&vm->st_call);
}

void vm_init_locals(VM *vm, PROC *method) {
	int i;
	for (i = 0; i < method->locals - method->num_args; i++)
		stack_arg_push(&vm->st_arg, vm_create_nil(vm));
}

void vm_init_pc(VM *vm, int argc, char **argv) {
	HTIter it;
	CLASS *c;
	PROC *main_proc = NULL;
	htable_iterator_init(&vm->classes, &it);
	while(main_proc == NULL && (c = (CLASS*)htable_iterator_obj(&it))) {
		main_proc = (PROC*)htable_find(&c->procs, "main");
		htable_iterator_next(&vm->classes, &it);
	}
	if (main_proc == NULL)
		VM_PERROR_EXIT("proc main not found in none of the classes\n");

	if (main_proc->num_args > 1)
		VM_PERROR_EXIT("proc main can take only one argument\n");

	OBJECT *main_obj = vm_create_instance(vm, c->this_name);
	vm->this = main_obj;
	vm->pc = proc_get_first_inst(main_proc);

	/* hack to avoid garbage collecting main's object */
	stack_arg_push(&vm->st_arg, vm->this);

	vm->ebp = 1;

	if (main_proc->num_args) {
		OBJECT *array = vm_create_array(vm, argc);
		int i;
		for (i = 0; i < argc; i++)
			array->array.data[i] = vm_create_string(vm, argv[i]);
		OBJECT *args = vm_create_instance(vm, "Array");
		args->field[0] = array;
		stack_arg_push(&vm->st_arg, args);
	} else {
		stack_arg_push(&vm->st_arg, vm_create_nil(vm));
	}

	vm->current = main_obj->head.class;
	vm->curr_proc = main_proc;

	vm_init_locals(vm, main_proc);
}

void vm_resolve_const(VM *vm) {
	HTIter it;
	CLASS *c;
	htable_iterator_init(&vm->classes, &it);
	while((c = (CLASS*)htable_iterator_obj(&it))) {
		struct literal *constant;
		int i;
		c->super = htable_find(&vm->classes, c->super_name);
		if (c->super == NULL)
			VM_PERROR_EXIT("unable to locate parrent class %s\n", c->super_name);
		for (i = 0; i < c->cons.size; i++) {
			constant = &c->cons.table[i];
			char *val = c->cons.table[i].name;
			DEBUG_PRINT("cons: %s ", val);
			switch(constant->type) {
				case C_INT:
					c->cons.table[i].obj = vm_create_int_from_string(vm, val);
					DEBUG_PRINT("INT addr: %p\n", c->cons.table[i].obj);
					break;
				case C_STRING: {
					char *escaped = str_escape(val);
					c->cons.table[i].obj = vm_create_string(vm, escaped);
					DEBUG_PRINT("STRING addr: %p val: %s\n", c->cons.table[i].obj,
									object_get_string(c->cons.table[i].obj));
					free(escaped);
					}
					break;
				case C_CLASS:
					c->cons.table[i].obj = vm_create_class(vm, val);
					DEBUG_PRINT("CLASS addr: %p\n", c->cons.table[i].obj);
					break;
				case C_METHOD:
					DEBUG_PRINT("METHOD\n");
					break;
				case C_NIL:
					c->cons.table[i].obj = vm_create_nil(vm);
				default:
					break;
			}
		}
		htable_iterator_next(&vm->classes, &it);
	}
}

void vm_run(VM *vm) {
	ASTACK *astack = &vm->st_arg;
	for (;;) {
		struct cons_table *consts = &vm->current->cons;
		int iop1 = 0, iop2 = 0;
		if (vm->pc->op1)
			iop1 = *(int*)vm->pc->op1;
		if (vm->pc->op2)
			iop2 = *(int*)vm->pc->op2;

		DEBUG_PRINT("PC: ");
		iprint(vm->pc);
		DEBUG_PRINT("CLASS: %s INSTANCE: %p method %s %d\n", vm->current->this_name, vm->this, "", vm->this->head.flags);
#ifdef _DEBUG
		if (debug) {	int i;
			for (i = 0; i < astack->top; i++) {
				printf("%d: %p", i, astack->data[i]);
				if (astack->data[i]) {
					printf(" %s", object_get_class(astack->data[i])->this_name);
					printf(" %d", ((OBJECT*)astack->data[i])->head.flags);
				}
				printf("\n");
			}
		}
#endif
		DEBUG_PRINT("ebp: %d\n", vm->ebp);
		if (stack_arg_top(astack))
			DEBUG_PRINT("top: %s\n", object_get_class(stack_arg_top(astack))->this_name);
#ifdef _DEBUG
		if (getenv("VM_DEBUG_STEP"))
			fgetc(stdin);
#endif
		switch(vm->pc->opcode) {
			case PUSH_LOCAL:
				stack_arg_push(astack, stack_arg_get(astack, vm->ebp + iop1));
				break;
			case PUSH_IVAR:
				stack_arg_push(astack, vm->this->field[iop1]);
				break;
			case PUSH_CONST:
				stack_arg_push(astack, cons_table_obj(consts, iop1));
				break;
			case SET_LOCAL:
				stack_arg_set(astack, vm->ebp + iop1, stack_arg_top(astack));
				break;
			case SET_IVAR:
				vm->this->field[iop1] = stack_arg_top(astack);
				break;
			case CALL_NATIVE:
				native_function[iop1](vm);
				break;
			case CALL_SUPER:
			case CALL: {
					CLASS *caller = object_get_class(stack_arg_top(astack));
					if (vm->pc->opcode == CALL_SUPER)
						caller = vm->current->super;

					const char *method_name = cons_table_name(consts, iop1);
					CLASS *callee = caller;
					PROC *target_proc = class_lookup_proc(&callee, method_name);
					if (target_proc == NULL)
						VM_PERROR_BT_EXIT(vm, "%s [%p] has no method %s\n",
							caller->this_name, stack_arg_top(astack), method_name);
					if (target_proc->num_args >= 0 && target_proc->num_args != iop2)
						VM_PERROR_BT_EXIT(vm, "%s.%s called with wrong number of arguments expected %d got %d\n",
		caller->this_name, target_proc->name, target_proc->num_args, iop2);

					CSTACK_ENTRY curr_state;
					curr_state.this = vm->this;
					curr_state.current_class = vm->current;
					curr_state.pc = vm->pc;
					curr_state.ebp = vm->ebp;
					curr_state.proc = vm->curr_proc;

					vm->current = callee;
					vm->curr_proc = target_proc;
					/* get next this */
					vm->this = stack_arg_pop(astack);
					/* allocate locals */
					vm->ebp = astack->top - iop2; // - args
					DEBUG_PRINT("new ebp: %d (%d - %d)\n", vm->ebp, astack->top, iop2);
					// stack_arg_alloc(astack, p->locals - iop2);
					vm_init_locals(vm, target_proc);

					DEBUG_PRINT("allocat: %d (%d - %d)\n", target_proc->locals - iop2, target_proc->locals, iop2);
					stack_call_push(&vm->st_call, curr_state);
					struct Inst *next_pc = proc_get_first_inst(target_proc);
					vm->pc = next_pc-1;
				}
				break;
			case PUSH_SELF:
					stack_arg_push(astack, vm->this);
				break;
			case POP:
					stack_arg_pop(astack);
				break;
			case RET: {
					if (vm->st_call.top == 0) {
						return;
					}
					OBJECT *ret;
					if (astack->top == vm->ebp)
						ret = vm->this;
					else
						ret = stack_arg_pop(astack);
					CSTACK_ENTRY old_state = stack_call_pop(&vm->st_call);
					vm->this = old_state.this;
					astack->top = vm->ebp;
					stack_arg_push(astack, ret);
					vm->pc = old_state.pc;
					vm->ebp = old_state.ebp;
					vm->current = old_state.current_class;
					vm->curr_proc = old_state.proc;
				}
				break;
			case JCMP: {
					OBJECT *res = stack_arg_pop(&vm->st_arg);
					DEBUG_PRINT("res: %s\n", mpz_get_str(NULL, 10, res->big_num));
					if (mpz_cmp_si(res->big_num, 0U) == 0) {
						vm->pc += iop1 - 1;
						DEBUG_PRINT("jumped\n");
					}
				}
				break;
			case JMP:
					vm->pc += iop1 - 1;
				break;
			default:
				VM_PERROR_BT_EXIT(vm, "unknown instruction opcode: %d\n", vm->pc->opcode);
				break;
		}
		vm->pc++;
	}
}

static FILE *file_open(const char *file) {
	FILE *fh = fopen(file, "r");
	if (fh == NULL) {
		fprintf(stderr, "opening: %s\n", file);
		perror("fopen");
		exit(EXIT_FAILURE);
	}
	return fh;
}

int main(int argc, char **argv) {

	DEBUG_INIT();

	VM vm;
	vm_init(&vm);

    int i;
	for (i = 0; i < sizeof(native_classes)/sizeof(native_classes[0]); i++) {
		char *native = getenv("VM_NATIVE");
		if (native == NULL)
			native = "./native";
		char *path = NULL;
		asprintf(&path, "%s/%s", native, native_classes[i]);
		CLASS *c = parse_load_class(file_open(path));
		htable_add(&vm.classes, c->this_name, c);
	}

    for (i = 1; i < argc; i++) {
		if (strcmp(argv[i], "-") == 0)
			break;
		CLASS *c = parse_load_class(file_open(argv[i]));
		if (htable_find(&vm.classes, c->this_name))
			VM_PERROR_EXIT("class %s already loaded\n", c->this_name);
		htable_add(&vm.classes, c->this_name, c);
	}
	i++;
	/* argv[i] now points to first argument of interpreted program */
	char **args = NULL;
	int num_args = 0;
	if (i < argc) {
		args = &argv[i];
		num_args = argc - i;
	}

	vm_resolve_const(&vm);
	vm_init_pc(&vm, num_args, args);
	vm_run(&vm);

	alloc_unmark(&vm.mem_alloc);
	alloc_collect(&vm.mem_alloc);
#ifdef _DEBUG
	if (getenv("VM_DEBUG_MEM")) {
		printf("\n");
		alloc_print_lists(&vm.mem_alloc);
	}
#endif
	vm_free(&vm);
	return EXIT_SUCCESS;
}

/* these are native functions */

void nf_new(VM *vm) {
	OBJECT *name = stack_arg_pop(&vm->st_arg);
	char *class_name = object_get_string(name);
	OBJECT *new = vm_create_instance(vm, class_name);
	stack_arg_push(&vm->st_arg, new);
}

void nf_sys_exit(VM *vm) {
	OBJECT *ret = stack_arg_pop(&vm->st_arg);
	int ret_val = (int)mpz_get_si(ret->big_num);
	exit(ret_val);
}

void nf_sys_out(VM *vm) {
	OBJECT *out = stack_arg_pop(&vm->st_arg);
	printf("%s", object_get_string(out));
}

void nf_sys_in(VM *vm) {
	char *input = NULL;
	size_t bytes = 0;
	if (getline(&input, &bytes, stdin) < 0) {
		perror("getline");
		printf("nf_in\n");
		input = "";
	}
	OBJECT *str_obj = vm_create_string(vm, input);
	stack_arg_push(&vm->st_arg, str_obj);
}

void nf_str_at(VM *vm) {
	char *str = object_get_string(vm->this);
	OBJECT *i = stack_arg_pop(&vm->st_arg);
	int index = (int)mpz_get_si(i->big_num);
	char subchar[2] = { '\0', '\0' };
	subchar[0] = str[index];
	OBJECT *new = vm_create_string(vm, &subchar[0]);
	stack_arg_push(&vm->st_arg, new);
}

void nf_concat(VM *vm) {
	char *s1 = object_get_string(vm->this);
	OBJECT *obj_str2 = stack_arg_pop(&vm->st_arg);
	char *s2 = &obj_str2->str;
	char *new_str = NULL;
	asprintf(&new_str, "%s%s", s1, s2);
	OBJECT *new = vm_create_string(vm, new_str);
	stack_arg_push(&vm->st_arg, new);
}

void nf_int_to_str(VM *vm) {
	OBJECT *integer = stack_arg_pop(&vm->st_arg);
	char *value = mpz_get_str(NULL, 10, integer->big_num);
	OBJECT *string = vm_create_string(vm, value);
	stack_arg_push(&vm->st_arg, string);
	free(value);
}

void nf_int_add(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	mpz_add(ir->big_num, i1->big_num, i2->big_num);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_sub(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	mpz_sub(ir->big_num, i1->big_num, i2->big_num);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_mul(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	mpz_mul(ir->big_num, i1->big_num, i2->big_num);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_div(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	if(mpz_cmp_si(i2->big_num, 0) == 0)
		VM_PERROR_BT_EXIT(vm, "division by zero\n");
	OBJECT *ir = vm_create_int(vm);
	mpz_div(ir->big_num, i1->big_num, i2->big_num);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_le(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	int ret = mpz_cmp(i1->big_num, i2->big_num);
	mpz_set_si(ir->big_num, ret <= 0);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_ge(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	int ret = mpz_cmp(i1->big_num, i2->big_num);
	mpz_set_si(ir->big_num, ret >= 0);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_lt(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	int ret = mpz_cmp(i1->big_num, i2->big_num);
	DEBUG_PRINT("int_lt: %s %s\n", mpz_get_str(NULL, 10, i1->big_num),
				mpz_get_str(NULL, 10, i2->big_num));
	mpz_set_si(ir->big_num, ret < 0);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_gt(VM *vm) {
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	int ret = mpz_cmp(i1->big_num, i2->big_num);
	mpz_set_si(ir->big_num, ret > 0);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_eq(VM *vm) {
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	int ret = mpz_cmp(i1->big_num, i2->big_num);
	mpz_set_si(ir->big_num, ret == 0);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_ne(VM *vm) {
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *i2 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	int ret = mpz_cmp(i1->big_num, i2->big_num);
	mpz_set_si(ir->big_num, ret != 0);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_is_nil(VM *vm) {
	OBJECT *i = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	mpz_set_si(ir->big_num, i->head.flags & FLAG_NIL);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_int_min(VM *vm) {
	OBJECT *i1 = stack_arg_pop(&vm->st_arg);
	OBJECT *ir = vm_create_int(vm);
	mpz_neg(ir->big_num, i1->big_num);
	stack_arg_push(&vm->st_arg, ir);
}

void nf_str_cmp(VM *vm) {
	OBJECT *s1 = stack_arg_pop(&vm->st_arg);
	OBJECT *s2 = stack_arg_pop(&vm->st_arg);
	char *v1 = object_get_string(s1);
	char *v2 = object_get_string(s2);
	OBJECT *res = vm_create_int(vm);
	mpz_set_si(res->big_num, strcmp(v1, v2) == 0);
	stack_arg_push(&vm->st_arg, res);
}

void nf_str_length(VM *vm) {
	OBJECT *s = stack_arg_pop(&vm->st_arg);
	int length = strlen(object_get_string(s));
	OBJECT *res = vm_create_int(vm);
	mpz_set_si(res->big_num, length);
	stack_arg_push(&vm->st_arg, res);
}

void nf_void_type(VM *vm) {
	OBJECT *s = stack_arg_pop(&vm->st_arg);
	CLASS *c = object_get_class(s);
	stack_arg_push(&vm->st_arg, vm_create_string(vm, c->this_name));
}

void nf_addr_to_int(VM *vm) {
	OBJECT *s = stack_arg_pop(&vm->st_arg);
	OBJECT *res = vm_create_int(vm);
	mpz_set_ui(res->big_num, (size_t)s);
	stack_arg_push(&vm->st_arg, res);
}

void nf_str_to_int(VM *vm) {
	OBJECT *s = stack_arg_pop(&vm->st_arg);
	char *str = object_get_string(s);
	stack_arg_push(&vm->st_arg, vm_create_int_from_string(vm, str));
}

void nf_int_to_hex_str(VM *vm) {
	OBJECT *s = stack_arg_pop(&vm->st_arg);
	char *hex = mpz_get_str(NULL, 16, s->big_num);
	char *str = NULL;
	asprintf(&str, "0x%s", hex);
	stack_arg_push(&vm->st_arg, vm_create_string(vm, str));
	free(str);
	free(hex);
}

void nf_array_new(VM *vm) {
	OBJECT *i = stack_arg_pop(&vm->st_arg);
	int long size = mpz_get_si(i->big_num);
	stack_arg_push(&vm->st_arg, vm_create_array(vm, size));
}

void nf_array_index(VM *vm) {
	OBJECT *i = stack_arg_pop(&vm->st_arg);
	OBJECT *a = stack_arg_pop(&vm->st_arg);
	int long index = mpz_get_si(i->big_num);
	int size = a->array.size;
	OBJECT *ret = a;
	if (index >= 0 && index < size) {
		void *val = a->array.data[index];
		if (val != NULL)
			ret = val;
	}
	stack_arg_push(&vm->st_arg, ret);
}


void nf_array_write(VM *vm) {
	//TODO: allocating exact size is not very
	// efficient (a lot of reallocations)
	OBJECT *v = stack_arg_pop(&vm->st_arg);
	OBJECT *i = stack_arg_pop(&vm->st_arg);
	OBJECT *a = stack_arg_pop(&vm->st_arg);
	int long index = mpz_get_si(i->big_num);
	int size = a->array.size;
	OBJECT *ret = a;
	if (index >= 0) {
		if (index < size) {
			a->array.data[index] = v;
		} else {
			ret = vm_create_array(vm, index + 1);
			int new_size = ret->array.size;
			int j;
			for (j = 0; j < size; j++)
				ret->array.data[j] = a->array.data[j];
//			for (; j < new_size - 1; j++)
//				ret->array.data[j] = vm_create_nil(vm);
			ret->array.data[new_size - 1] = v;
		}
	}
	stack_arg_push(&vm->st_arg, ret);
}

void nf_array_length(VM *vm) {
	OBJECT *a = stack_arg_pop(&vm->st_arg);
	OBJECT *res = vm_create_int(vm);
	mpz_set_si(res->big_num, a->array.size);
	stack_arg_push(&vm->st_arg, res);
}

void nf_sys_bt(VM *vm) {
	vm_print_bt(vm);
}

void nf_assert_string(VM *vm) {
	OBJECT *s = stack_arg_top(&vm->st_arg);
	if (!(s->head.flags & FLAG_STRING)) {
		printf("%d\n", s->head.flags);
		VM_PERROR_BT_EXIT(vm, "can't convert to string\n");
	}
}

void nf_assert_int(VM *vm) {
	OBJECT *s = stack_arg_top(&vm->st_arg);
	if (!(s->head.flags & FLAG_INT))
		VM_PERROR_BT_EXIT(vm, "can't convert to int\n");
}

static void (*native_function[])(VM *vm) = {
	FOREACH_NATIVE(GENERATE_NATIVE_FNC)
};
