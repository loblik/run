#include <string.h>
#include "native.h"

char *native_str[] = {
	FOREACH_NATIVE(GENERATE_NATIVE_STRING)
};

int native_index(char *native_name) {
	native_name++;
	int i;
	for (i = 0; i < sizeof(native_str)/sizeof(native_str[0]); i++) {
		if (strcmp(native_str[i], native_name) == 0) {
			return i;
		}
	}
	return -1;
}

