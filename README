RUNTIME SYSTEMY - POPIS IPLEMENTACE
-----------------------------------

ZAKLADNI VLASTNOSTI
-------------------

- vlastni objektovy jazyk inspirovany Ruby
- slabe typovany
- jednoducha dedicnost
- temer vsechno je objekt, vse reseno pomoci volani metod
- heap je ve vlastni strance, alokace bloku buddy system algoritmem
- mark and sweep GC
- libovolne velka cisla (knihovna gmp)
- jednoduche pretezovani metod, pocet argumentu se pri definici a volani metody ulozi do nazvu
	foo(a) -> foo_1,  foo() -> foo_0
- instancni promenne pouze privatni, ale lze pouzit pretezovani nasledovne
	zapis			    prelozeno jako
	-----------------------------------
	obj.foo = bar		obj.foo(bar)
	bar = obj.foo		bar = obj.foo()

- operatory (preklad na metody)
	operator			metoda
	-----------------------------------
	+					add
	-					sub
	*					mul
	/					div
	- (unarni)			min
	<=					le
	>=					ge
	<					lt
	>					gt
	==					eq
	!=					ne
	&&					and
	||					or
	[]					at
	a[i]=foo			a.write(i, foo)

- zakladni typy:
	Zakladni typy String, Int, Array, Nil, Void (zakladni trida) jsou implementovany pomoci
nativnich funkci v prislusnych tridach. Viz napr. native/[String.lp|Array.lp].

- konverze
	Pokud se objekt predava nekam kde se ocekava Int pak se na nej zavola metoda to_int
pokud se ocekava String pak to_str. Pote vm zkontroluje pomoci _ASSERT_* nativni funkce
zda ke konverzi skutecne doslo a pripadne skonci s chybou a vypisem stacku.

- retezce:
	Lze pouzit specialni znaky \r\n\t\f\b, unicode (\u..) escapovani neni podporovano.

IMPLEMENTACE
------------
- konstanty
	Kazda trida obsahuje tabulku konstant (C_INT, C_STRING, C_CLASS, C_METHOD, C_NIL).
Tyto tabulky jsou vyplneny pri prekladu a instrukce v kazde tride se odkazuji indexy do
svych tabulek. Po spusteni vm pro kazdou konstantu vytvori objekt a do tabulky doplni jeho
referenci.

- instancni promene
	Instanci promene jsou reseny pouze offsety. Prvni promena tridy je ulozena na
offsetu za offsetem posledni instancni promene nadtridy. Pri zmene polozek je tedy
nutne provest rekompilaci vsech podtrid.

- spusteni
	Po spusteni vm dojde k naplneni tabulek konstant objekty a vyhledani tridy ktera
obsahuje metodu main. Pote dojde k incializaci zasobniku, pripadne vytvoreni pole
argumentu z prikazove radky a spusteni kodu metody. V tomto jedinem pripade nedochazi
automaticky ke spusteni metody init.

- sprava pameti a GC
	VM si alokuje zarovnany blok fixni delky pomoci posix_memalign. Tento blok
spravuje pomoc buddy system algoritmu. Pokud jiz neexistuje volny blok dane velikosti
spusti se mark and sweep na vsechny koreny (hlavni stack, this, this na call stacku) a
rekurzivne se oznaci vsechny dostupne objekty. Potom se projde seznam pridelenych bloku
a a neznacene se vraci zpet mezi volne. Neni li stale k dispoziti volny blok program
se ukonci.

SOUBORY
-------
 - parser
	parser.y: pravidela gramatiky a jednotlive akce pro staveni stromu
	ast.c: implementace stromu, funkce .*_print generujou kod a vypisujou class soubor
 - vm
	class.c: trida
	class_parse.c: parsovani JSONu a vytvareni trid
	code.c: bytecode
	cons.c: tabulka constant
	hash.c: hashovaci tabulka
	ident.c: tabulka identifikatoru pro parsovani
	stack_arg.c: hlavni zasobnik
	native.h: mapovani jmen nativnich funkci na C funkce
	vm.c: hlavni cast vm

 - ostatni
	bin - vysledne binarky, pomocne skripty
	native - nativni metody
	examples - skripty s priklady

ZAVISLOSTI A KOMPILACE
----------------------
	Projekt by mel byt prelozitelny na jakemkoliv POSIX systemu, je potreba mit flex a bison
pro vygenerovani parseru a nainstalovanou knihovnu gmp (vcetne hlavicek). Dale je vm zavisla
na knihovne jsmn pro parsovani JSONu, ta je uz ale soucasti archivu.

 - pro preklad
	$ make
   nebo
	$ make debug

JAK TO FUNGUJE
--------------
	Po sestaveni vzniknou dve binarky compile a vm. Parser (compile) nacita *.lp
soubor a vygeneruje pro kazdou tridu jeden .class soubor v JSON formatu. Interpret (vm)
pak predany *class soubory interpretuje.

	priklad:
		./compile < foo.lp
		./vm Foo.class SuperFoo.class ...

JAK TO POUZIVAT
---------------
	Interpret potrebuje ke behu prelozene tridy zakladnich objektu (String, Int..).
Ty jsou nacitany z cesty urcene promenou VM_NATIVE, jinak z ./native.

	Nejprve pridame ./bin v korenovem adresari projektu do path.

	$ export PATH=$PATH:$PWD/bin

	Dale nastavime cestu k nativnim tridam.

	$ export VM_NATIVE=$PWD/native

	Prelozime nativni tridy do bytekodu.

	$ cd native
	$ compile_all.sh

	Ted lze jiz spoustet jednotlive priklady v ./examples. Hashbang nevyhledava
interpret v PATH. Proto je ve vsech prikladech uvedeno #!./vm.sh a ./bin/vm.sh
je nasymlinkovan do daneho adresare.

DEBUGOVANI
----------
	Pokud je kod prelozen s -D_DEBUG (make debug) pak lze pomoci export VM_DEBUG=1,
unset VM_DEBUG zapout/vypout ladici vypisy. Pri nastavenem VM_DEBUG_STEP=1 lze krokovat
vykonovani kodu stiskem klavesy. Pri nastaveni VM_DEBUG_MEM vm vypise pred ukoncenim stav pameti.

	Pro debugovani interpretovaneho kodu lze pouzit metodu Sys.new.bt.

MOZNA VYLEPSENI
---------------
- class metody? plnohodnotny class objekty na heapu?
- closures?
- vyjimky?
