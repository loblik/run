#ifndef HASH_H
#define HASH_H

/* TODO: auto resizing using primes etc. */

struct Entry;

struct HTable {
	struct Entry **table;
	unsigned int size_index;
	unsigned int entries;
	void *(*alloc)(size_t);
	void (*delete)(void*);
	/* TODO
	 size, hash function callback */
};

struct Entry {
	void *data;
	const char *key;
	struct Entry *next;
};

struct HTIter {
	struct Entry *p;
	int index;
};

typedef struct HTIter HTIter;
typedef struct HTable HTable;
typedef struct Entry Entry;

void htable_iterator_next(HTable *tab, HTIter *iter);
void htable_iterator_init(HTable*, HTIter*);
Entry *htable_iterator_entry(HTIter *iter);
void *htable_iterator_obj(HTIter *iter);

void htable_init(HTable *tab, void *(*allocator)(size_t), void (*delete)(void*));
void *htable_find(HTable *tab, const char *key);
void htable_remove(HTable *tab, const char *key);
void htable_free(HTable *tab);

void htable_foreach(HTable *tab, void (callback)(const char*, void*));
int htable_add(HTable *tab, const char *key, void *data);
#endif
